import './Footer.css';
import vk from "../../images/vk-icon.png";
import fb from "../../images/fb-icon.png";
import ig from "../../images/ig-icon.png";

const Footer = () => {
    return (
        <footer className={'footer'}>
            <div className={'footer__container'}>
                <div className={'footer__section'}>
                    <h1>Барбершоп Бородинский</h1>
                    <p>г. Санкт-Петербург, ул. Большая Конюшенная 19/8</p>
                    <p>Тел.: +7 (812) 555-66-66</p>
                </div>
                <div className={'footer__section'}>
                    <h2 className={'footer__socials-title'}>Давайте дружить!</h2>
                    <div className={'footer__socials'}>
                        <a href="https://vk.com" target={'_blank'} rel={'nofollow noopener noreferrer'}><img src={vk} alt="vkontakte-icon" className={'social-icon'}/></a>
                        <a href="https://facebook.com" target={'_blank'} rel={'nofollow noopener noreferrer'}><img src={fb} alt="facebook-icon" className={'social-icon'}/></a>
                        <a href="https://instagram.com" target={'_blank'} rel={'nofollow noopener noreferrer'}><img src={ig} alt="instagram-icon" className={'social-icon'}/></a>
                    </div>
                </div>
                <div className={'footer__section'}>
                    <h2>Разработано: </h2>
                    <div>
                        <a href="https://htmlacademy.ru" target={'_blank'} rel={'nofollow noopener noreferrer'} className={'developer-anchor'}>htmlacademy</a>
                    </div>
                </div>
            </div>
        </footer>
    )
}
export default Footer;