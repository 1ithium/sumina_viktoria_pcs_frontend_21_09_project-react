import Contacts from "../Contacts/Contacts";
import BackgroundMini from "../Background/BackgroundMini";

const ContactsPage = () => {

    return (
        <div className={'container'}>
            <BackgroundMini/>
            <Contacts />
        </div>
    )
}

export default ContactsPage;
