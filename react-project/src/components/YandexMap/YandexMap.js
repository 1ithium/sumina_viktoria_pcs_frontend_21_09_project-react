import { YMaps, Map, Placemark } from "react-yandex-maps";
import './YandexMap.css'

const mapData = {
    center: [59.938635, 30.323118],
    zoom: 15,
};

const coordinates = [
    [59.938635, 30.323118]
];

const YandexMap = ({width,height}) => (
    <YMaps className={'map__container'}>
        <Map defaultState={mapData} id={'map'} className={'map'}>
            {coordinates.map(coordinate => <Placemark geometry={coordinate} />)}
        </Map>
    </YMaps>
);


export default YandexMap;