import Background from "../Background/Background";
import Statistics from "../Statistics/Statistics";
import Features from "../Features/Features";
import Reviews from "../Reviews/Reviews";

const IndexPage = () => {
    return (
        <div className="container">
            <Background />
            <Statistics />
            <Features />
            <Reviews />
        </div>
    )
}

export default IndexPage;