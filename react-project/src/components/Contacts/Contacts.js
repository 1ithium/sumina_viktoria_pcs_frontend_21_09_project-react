import './Contacts.css';
import YandexMap from "../YandexMap/YandexMap";

const Contacts = () => {
    return (
        <section className={'contacts'}>
            <div className={'contacts__container'}>
                <h2 className={'contacts__title'}>Контакты</h2>
                <div className={'contacts__card-container'}>
                    <div className={'contacts__map'}>
                        <YandexMap></YandexMap>
                    </div>
                    <div className={'contacts__info'}>
                        <h1>Барбершоп Бородинский</h1>
                        <p>г. Санкт-Петербург, ул. Большая Конюшенная 19/8</p>
                        <p>Тел.: +7 (812) 555-66-66</p>
                    </div>
                </div>
            </div>
        </section>
    )
}
export default Contacts;