import './Features.css';
import featureIcon1 from "../../images/advantage-1-icon.png";
import featureIcon2 from "../../images/advantage-2-icon.png";
import featureIcon3 from "../../images/advantage-3-icon.png";

const Features = () => {
    return (
        <section className={'features'}>
            <div className={'features__container'}>
                <div className={'features__card'}>
                    <img src={featureIcon1} alt="feature-icon" className={'features__icon'}/>
                    <div className={'features__card-wrapper'}>
                        <h3>Быстро</h3>
                        <p>Мы делаем свою работу быстро! Два часа пролетят незаметно и вы — счастливый обладатель стильной стрижки-минутки</p>
                    </div>
                </div>
                <div className={'features__card'}>
                    <img src={featureIcon2} alt="feature-icon" className={'features__icon'}/>
                    <div className={'features__card-wrapper'}>
                        <h3>Круто</h3>
                        <p>Забудьте, как вы стриглись раньше. Мы сделаем из вас звезду футбола или кино. Во всяком случае внешне.</p>
                    </div>
                </div>
                <div className={'features__card'}>
                    <img src={featureIcon3} alt="feature-icon" className={'features__icon'}/>
                    <div className={'features__card-wrapper'}>
                        <h3>Дорого</h3>
                        <p>Наши мастера — профессионалы своего дела не могут стоить дёшево. К тому же, разве цена не даёт определённый статус?</p>
                    </div>
                </div>
            </div>
        </section>
    )
}
export default Features;