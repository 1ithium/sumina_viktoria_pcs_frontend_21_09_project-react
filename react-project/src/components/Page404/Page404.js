import './Page404.css';

const Page404 = () => {
    return (
        <section className={'page404'}>
            <div className={'page404__container'}>
                <h2 className={'page404__title'}>404. Страница не найдена</h2>
                <div>
                    <div className={'page404__error-description'}>
                        <p>Данный раздел пока находится в разработке.</p>
                        <p>Попробуйте вернуться на главную страницу.</p>
                    </div>
                </div>
            </div>
        </section>
    )
}
export default Page404;