import Form from "../Form/Form";
import BackgroundMini from "../Background/BackgroundMini";

const AppointmentPage = () => {
    return (
        <div className="container">
            <BackgroundMini/>
            <Form/>
        </div>
    )
}

export default AppointmentPage;