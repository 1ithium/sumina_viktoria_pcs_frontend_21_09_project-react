import './Statistics.css';

const Statistics = () => {
    return (
        <section className={'statistics'}>
            <div className={'statistics__container'}>
                <div className={'statistics__description'}>
                    <h2 className={'statistics__title'}>Стрижка у нас — это выгодно!</h2>
                    <p>Мужская стрижка требует
                    точного подхода. Обратимся
                    к статистике:</p>
                    <span>* — приведённые данные ложь</span>
                </div>
                <div className={'statistics__content'}>
                    <div className={'statistics__card'}>
                        <p className={'statistics__number'}>1 500 *</p>
                        <span>рублей вложений</span>
                    </div>
                    <div className={'statistics__card'}>
                        <p className={'statistics__number'}>90 000</p>
                        <span>постриженных волос</span>
                    </div>
                    <div className={'statistics__card'}>
                        <p className={'statistics__number'}>7 200</p>
                        <span>секунд времени мастера</span>
                    </div>
                    <div className={'statistics__card'}>
                        <p className={'statistics__number'}>500 000 *</p>
                        <span>лайков и комплиментов</span>
                    </div>
                </div>
            </div>
        </section>
    )
}
export default Statistics;