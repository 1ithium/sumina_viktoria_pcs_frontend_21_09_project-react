import './Reviews.css';
import photo1 from "../../images/photo1.png";
import photo2 from "../../images/photo2.png";
import photo3 from "../../images/photo3.png";

const Reviews = () => {
    const buttonPrev = document.querySelector('.reviews__button_back');
    const buttonNext = document.querySelector('.reviews__button_forward');

    let reviews = {
        id: [0,1,2],
        image: [photo1,photo2,photo3],
        title: ['Трэвис Баркер','Джон Баркер','Блэк Баркер'],
        text: ['Спасибо за лысину! Был проездом в Москве, заскочил побриться, чтобы было видно новую татуировку!','Был проездом в Москве, заскочил побриться, чтобы было видно новую татуировку!','Отличный сервис. Все понравилось!']
    };

    let reviewsLength = reviews.id.length;
    let i = 0;

    if (buttonNext) {
        buttonNext.addEventListener('click',ev => {
            if (reviewsLength < i) {
                console.log(i);
                i++;
            }
        });
    }
    console.log(i);
    if (buttonNext) {
        buttonPrev.addEventListener('click',ev => {
            if (reviewsLength > i) {
                i--;
            }
        });
    }
    console.log(i);
    return (
        <section className={'reviews'}>
            <div className={'reviews__container'}>
                <h2 className={'reviews__title'}>Отзывы о нас</h2>
                <div className={'reviews__card-container'}>
                    <button className={'reviews__button reviews__button_back'}> </button>
                    <div className={'reviews__card-wrapper'}>
                        <img src={reviews.image[i]} alt={''} className={'reviews__card-image'}/>
                        <div className={'reviews__card'}>
                            <h3 className={'reviews__card-title'}>{reviews.title[i]}</h3>
                            <p className={'reviews__card-review'}>{reviews.text[i]}</p>
                        </div>
                    </div>
                    <button className={'reviews__button reviews__button_forward'}> </button>
                </div>
            </div>
        </section>
    )
}

export default Reviews;