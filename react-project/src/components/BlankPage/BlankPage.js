import BackgroundMini from "../Background/BackgroundMini";
import Page404 from "../Page404/Page404";

const BlankPage = () => {
    return (
        <div>
            <BackgroundMini/>
            <Page404 />
        </div>
    )
}
export default BlankPage;