import './Header.css';
import {Link, NavLink} from "react-router-dom";

const Header = () => {
    return (
        <header className={'header'}>
            <div className={'header__menu-container'}>
                <nav className={'header__menu'}>
                    <ul>
                        <li className={'header__menu-item'}>
                            <NavLink to={'/'} className={'header__menu-anchor'} activeClassName={'active'}>Главная</NavLink>
                        </li>
                        <li className={'header__menu-item'}>
                            <NavLink to={'works'} className={'header__menu-anchor'} activeClassName={'active'}>Наши работы</NavLink>
                        </li>
                        <li className={'header__menu-item'}>
                            <NavLink to={'appointment'} className={'header__menu-anchor'} activeClassName={'active'}>Записаться</NavLink>
                        </li>
                        <li className={'header__menu-item'}>
                            <NavLink to={'contacts'} className={'header__menu-anchor'} activeClassName={'active'}>Контакты</NavLink>
                        </li>
                        <li className={'header__menu-item header__login'}>
                            <NavLink to={'login'} className={'header__menu-anchor'} activeClassName={'active'}>Войти</NavLink>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>
    )
}

export default Header;
