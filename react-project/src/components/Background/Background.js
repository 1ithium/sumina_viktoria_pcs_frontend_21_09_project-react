import './Background.css';
import logo from "../../images/logo.png";

const Background = () => {
    return (
        <div className={'back'}>
            <div className={'back__logo'}>
                <img src={logo} alt="logo" />
            </div>
        </div>
    )
}

export default Background;
