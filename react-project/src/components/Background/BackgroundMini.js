import './Background.css';
import logo from "../../images/logo.png";

const BackgroundMini = () => {
    return (
        <div className={'back'} style={{minHeight: 260}}>
            <div className={'back__logo'}>
                <img src={logo} alt="logo" />
            </div>
        </div>
    )
}

export default BackgroundMini;
