import Reviews from "../Reviews/Reviews";

const WorksPage = () => {
    return (
        <div className="container">
            <Reviews />
        </div>
    )
}

export default WorksPage;