import './Form.css';
import Button from "../Button/Button";

const Form = () => {
    return (
        <section className={'form'}>
            <div className={'form__container'}>
                <h2 className={'form__title'}>Запись на стрижку</h2>
                <div className={'form__input-container'}>
                    <form className={'form__wrapper'}>
                        <div className={'form__input-wrapper'}>
                            <input type={'text'} className={'form__input'} placeholder={'Фамилия'} required/>
                            <input type={'text'} className={'form__input'} placeholder={'Имя'} required/>
                            <input type={'text'} className={'form__input'} placeholder={'Отчество'}/>
                        </div>
                        <div className={'form__input-wrapper'}>
                            <input type={'tel'} className={'form__input form__input_contacts'} placeholder={'Контактный телефон'} required/>
                            <input type={'email'} className={'form__input form__input_contacts'} placeholder={'Контактный e-mail'}/>
                        </div>
                        <textarea className={'form__input form__input_large'} placeholder={'Доп. информация для мастера'}/>
                        <Button text={'Отправить заявку'}/>
                    </form>
                </div>
            </div>
        </section>
    )
}
export default Form;