import './App.css';
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import {Outlet, Route, Routes} from "react-router-dom";
import IndexPage from "./components/IndexPage/IndexPage";
import WorksPage from "./components/WorksPage/WorksPage";
import AppointmentPage from "./components/AppointmentPage/AppointmentPage";
import ContactsPage from "./components/ContactsPage/ContactsPage";
import BlankPage from "./components/BlankPage/BlankPage";

function App() {
    return (
        <Routes>
            <Route path={'/'} element={<Layout/>}>
                <Route index element={<IndexPage/>}/>
                <Route path={'works'} element={<BlankPage/>}/>
                <Route path={'appointment'} element={<AppointmentPage/>}/>
                <Route path={'contacts'} element={<ContactsPage/>}/>
                <Route path={'login'} element={<BlankPage/>}/>
            </Route>
        </Routes>
    );
}

function Layout() {
    return (
        <div className="App">
            <Header />
            <main className={'content'}>
                <Outlet/>
            </main>
            <Footer />
        </div>
    )
}

export default App;
